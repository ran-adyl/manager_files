<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get('/','UploadFileController@index')->name('load_dir');
Route::get('/add','UploadFileController@add')->name('add_dir');
Route::post('/store_cat','UploadFileController@store_cat')->name('store_dir');
Route::get('/edit','UploadFileController@edit')->name('edit_dir');
Route::get('/delete','UploadFileController@delete')->name('delete_dir');
Route::put('/update','UploadFileController@update')->name('update_dir');
Route::post('/info','UploadFileController@showUploadFile');

Route::get('/fil','FileController@index')->name('load_file');
Route::get('/fil_add','FileController@add')->name('add_file');
Route::post('/fil_store','FileController@store_cat')->name('store_file');
Route::get('/fil_edit','FileController@edit')->name('edit_file');
Route::get('/fil_delete','FileController@delete')->name('delete_file');
Route::put('/fil_update','FileController@update')->name('update_file');

Route::get('/download','DownloadsController@download')->name('download_file');