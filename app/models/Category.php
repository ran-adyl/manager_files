<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table='Categories';

    protected $fillable = [ 'name',  'path', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function files()
    {
        return $this->belongsTo('App\Models\File','dir_id','id');
    }

}
