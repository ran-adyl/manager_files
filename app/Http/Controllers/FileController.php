<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\File;


class FileController extends Controller
{
    public function index(Request $request){

        $id=$request->input('id');
        $categ=Category::query()->where(['id'=>$id])->first();
        $query = File::query()->where(['dir_id'=>$id]);

        $fileItem = $query->paginate(5);

        /*        if ($request->ajax()) {
                    return view('partials.return-items', ['returnItems' => $returnItem])->render();
                }*/
        return view('file.index', ['fileItems' => $fileItem,'category'=>$categ])->with('title','Файлы');
    }

    public function add(Request $request)

    {
        $id=$request->input('id');
        //$categ=Category::query()->where(['id'=>$id])->first();
        return view('file.add',['category_id'=>$id])->with('title','Файлы. Добавление');
    }

    public function store_cat(Request $request)
    {
        $file=$request->file('myfile');

        $id=$request->input('dir_id');

        $query=Category::query()->where(['id'=>$id])->first();

        $destinationPath=$query->path;

        $file1 = new File();
        $file1->name=$file->getClientOriginalName();
        $file1->dir_id=$id;
        $file1->extension=$file->getClientOriginalExtension();
        $file1->user_id=1;//$request->input('user_id');
        $file1->save();

        $file->move($destinationPath,$file->getClientOriginalName());

        return redirect('/');
    }
    public function edit(Request $request)
    {
        $id=$request->input('id');
        $requstItem=File::query()->where(['id'=>$id])->first();
        return view('file.edit', ['requestItem'=>$requstItem])->with('title','Файлы. Редактирование');
    }

    public function update(Request $request)
    {

        $id = $request->input('id');

        $request->input('name');

        $fileItem= File::query()->where(['id'=>$id])->first();

        $fileItem->name=$request->input('name');
        $fileItem->user_id=1;
        $fileItem->save();


        $fileItem->save();


        return redirect('/files');
    }

    public function delete(Request $request)
    {
        $id=$request->input('id');
        $requestItem=Category::query()->where(['id'=>$id])->first();
        $requestItem->delete();
        return redirect("/");
    }
    //
}
