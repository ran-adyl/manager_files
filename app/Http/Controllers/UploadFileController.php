<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Models\Category;
use App\Models\File;
use Illuminate\Http\Request;

class UploadFileController extends Controller
{
    public function index(){

        $query = Category::query();

        $categoryItem = $query->paginate(5);

/*        if ($request->ajax()) {
            return view('partials.return-items', ['returnItems' => $returnItem])->render();
        }*/
        return view('category.index', ['categoryItems' => $categoryItem])->with('title','Категории');
    }

    public function add()
    {
        return view('category.add')->with('title','Категории. Добавление');
    }

    public function store_cat(Request $request)
    {
        $name = $request->input('category');
        $destinationPath = 'files/'.$name;
        if (!file_exists($destinationPath)) {

            mkdir($destinationPath,0777);

            $category1 = new Category();
            $category1->name=$name;
            $category1->path=$destinationPath;
            $category1->user_id=1;//$request->input('user_id');
            $category1->save();
        }
        return redirect('/');
    }
    public function edit(Request $request)
    {
        $id=$request->input('id');
        $requstItem=Category::query()->where(['id'=>$id])->first();
        return view('category.edit', ['requestItem'=>$requstItem])->with('title','Категории. Редактирование');
    }

    public function update(Request $request)
    {

        $id = $request->input('id');

        $name = $request->input('name');

        $categoryItem= Category::query()->where(['id'=>$id])->first();

        $categoryItem->name=$name;
        $categoryItem->user_id=1;
        $categoryItem->save();


        $categoryItem->save();


        return redirect('/');
    }

    public function delete(Request $request)
    {
        $id=$request->input('id');
        $requestItem=Category::query()->where(['id'=>$id])->first();
        $requestItem->delete();
        return redirect("/");
    }

    public function showUploadFile(Request $request)
    {
        $file = $request->file('myfile1');

        $destinationPath = $request->input('category');
        $user_id=$request->input('user_id');

        // отображаем имя файла
        echo 'File Name: '.$file->getClientOriginalName();
        echo '<br>';

        //отображаем расширение файла
        echo 'File Extension: '.$file->getClientOriginalExtension();
        echo '<br>';

        //отображаем фактический путь к файлу
        echo 'File Real Path: '.$file->getRealPath();
        echo '<br>';

        //отображаем размер файла
        echo 'File Size: '.$file->getSize();
        echo '<br>';

        //отображаем Mime-тип файла
        echo 'File Mime Type: '.$file->getMimeType();
        echo '<br>';

        //перемещаем загруженный файл
//        $destinationPath = $file->getClientOriginalName();

        echo 'DestinationPath: '.$destinationPath;
        if (!file_exists($destinationPath)) {

            mkdir($destinationPath,0777);

            $category1 = new Category();
            $category1->name=$destinationPath;
            $category1->path=$destinationPath;
            $category1->user_id=1;//$request->input('user_id');
            $category1->save();
        }

        $file->move($destinationPath,$file->getClientOriginalName());


        $file1 = new File();
        $file1->name=$file->getClientOriginalName();
        $file1->path=$destinationPath;
        $file1->extension=$file->getClientOriginalExtension();
        $file1->user_id=1;//$request->input('user_id');
        $file1->save();
        //return dd($file1);
        return redirect('/');
    }
/*

public function add()
{
    $agents = Agent::query()->get();
    $cities = Citie::query()->get();
    $decisions = Decision::query()->get();
    $items = Item::query()->get();
    $reasons = reason::query()->get();
    return view('return_items.add', ['item' => $items, 'agent' => $agents, 'city' => $cities, 'decision' => $decisions,
        'reason' => $reasons])->with('title','Возврат продуктов. Добавление');
}

public function edit(Request $request)
{
    $agents = Agent::query()->get();
    $cities = Citie::query()->get();
    $decisions = Decision::query()->get();
    $items = Item::query()->get();
    $reasons = reason::query()->get();
    $id=$request->input('id');
    $requstItem=ReturnItem::query()->where(['id'=>$id])->first();
    return view('return_items.edit', ['item' => $items, 'agent' => $agents, 'city' => $cities, 'decision' => $decisions,
        'reason' => $reasons,'requestItem'=>$requstItem])->with('title','Возврат продуктов. Редактирование');
}

public function delete(Request $request)
{
    $id=$request->input('id');
    $requestItem=ReturnItem::query()->where(['id'=>$id])->first();
    $requestItem->delete();
    return redirect("/");
}
public function update(Request $request)
{

    $id = $request->input('id');
    $returnItem= ReturnItem::query()->where(['id'=>$id])->first();

    $returnItem->fill($request->input());


    $returnItem->save();


    return redirect('/');
}

public function store(Request $request)
{


    $returnItem = new ReturnItem();

    $returnItem->fill($request->input());

    $returnItem->save();
    return redirect('/');
}

*/
}