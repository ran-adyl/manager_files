<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\File;
use App\Models\Category;


class DownloadsController extends Controller
{
    //
    public function download(Request $request){
        $file_id=$request->input('file');

        $query=File::query()->where(['id'=>$file_id])->first();
        //dd($query);
        $query1=Category::query()->where(['id'=>$query->dir_id])->first();

        $file_path=public_path($query1->path.'/'.$query->name);

        //echo $file_path;

        return response()->download($file_path);
    }
}
