<!DOCTYPE html>
<!-- saved from url=(0039)http://lab.maselko.uz/ReturnItems/index -->
<html>
<head>
    <!--include('layouts.head')-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>  {{$title}} </title>
    <link href="http://lab.maselko.uz/favicon.ico" type="image/x-icon" rel="icon">
    <link href="http://lab.maselko.uz/favicon.ico" type="image/x-icon" rel="shortcut icon">
    <link rel="apple-touch-icon-precomposed" href="http://lab.maselko.uz/webroot/apple_touch_icon.png">

    <link rel="stylesheet" href={{asset("css/bootstrap.min.css")}}>
    <link rel="stylesheet" href={{asset("css/animate.css")}}>

</head>
<body>
<header>
    <!--include('layouts.header')-->
</header>
<br>
<!--


    <div class="jumbotron">
      <div class="container">

      </div>

    </div>-->

    @yield('content')

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/moment-with-locales.min.js"></script>
    <script src="js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $('#returned_date').datetimepicker({
                locale: 'ru',
                stepping: 10,
                format: 'YYYY-MM-DD',
                defaultDate: moment('01.01.2015').format('YYYY-MM-DD'),
                //daysOfWeekDisabled:[0,6]
            });
            $('#product_date').datetimepicker({
                locale: 'ru',
                stepping: 10,
                format: 'YYYY-MM-DD',
                defaultDate: moment('{{date("Y-m-d")}}').format('YYYY-MM-DD'),
//            daysOfWeekDisabled:[0,6]
            });
            $('#planned_date').datetimepicker({
                locale: 'ru',
                stepping: 10,
                format: 'YYYY.MM.DD',
                //defaultDate: moment('01.11.2017').format('DD.MM.YYYY'),
//            daysOfWeekDisabled:[0,6]
            });

        });
    </script>

</body></html>