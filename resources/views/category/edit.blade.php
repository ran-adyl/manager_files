@extends('layouts.main')
@section('content')
<div class="container">
    <!-- Example row of columns -->
    <div class="row">
        <div class="col-md-12">

            <div class="actions columns large-2 medium-3">
                <ul class="list-inline">
                    <li>
                        <form name="post_5b3373d009b46402802823" style="display:none;" method="post"
                              action="http://lab.maselko.uz/category_items/delete/48607">
                            <input type="hidden" name="_method" value="POST"></form>
                        <a href="{{route('delete_dir')}}?id={{$requestItem->id}}" class="btn btn-xs btn-danger"
                           onclick="if (confirm(&quot;Are you sure you want to delete # {{$requestItem->id}}?&quot;))
                                   { document.post_5b2dbb7bb7ab4047066095.submit(); }
                                   event.categoryValue = false;
                                   return false;
                                   ">Удалить</a>
                    <li><a href="{{route('load_dir')}}" class="btn btn-xs btn-success"><i class="icon-level-down"></i>
                            Список Категорий</a></li>
                </ul>
            </div>
        <!--                <?php print_r($requestItem->id) ?>-->
            <div class="categoryItems form large-10 medium-9 columns">
                <form method="post" accept-charset="utf-8" role="form" action="{{route('update_dir')}}">
                    <div style="display:none;"><input type="hidden" name="_method" value="PUT"></div>
                    <fieldset>
                        <legend>Изменить Категорию</legend>

                        <div class="form-group"><label class="control-label" for="quantity">Наименование</label>
                            <input type="text" name="name" id="name"
                                   value={{$requestItem->name}} class="form-control"></div>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="id" value="{{$requestItem->id}}">
                        <input type="hidden" name="user_id" value="1">
                        <!--                <input type="number" name="quantity_category_item" id="quantity-category-item" class="form-control"></div>-->


                    </fieldset>
                    <button type="submit" class="btn">Отправить</button>
                </form>
            </div>
        </div>
    </div>

    <hr>

    <footer>
        <p>© Maselko 2016</p>
    </footer>
</div> <!-- /container -->
@endsection