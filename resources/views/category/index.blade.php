    @extends('layouts.main')
    @section('content')
    <div class="container">
        <!-- Example row of columns -->
        <div class="row">
            <div class="col-md-12">

                <div class="actions columns large-2 medium-3">

    <ul class="list-inline">
        <li><a href="{{route('add_dir')}}" class="btn btn-xs btn-primary"><i class="icon-plus" ></i> Добавить Категорию</a></li>
<!--        <li><a href="http://lab.maselko.uz/.csv" class="btn btn-xs btn-warning"><i class="icon-plus"></i> Экспорт CSV</a></li>-->
    </ul>
</div>


<div class="categoryItems index large-10 medium-9 columns">
    <table class="table table-striped">
    <thead>
    <!-- НАЗВАНИЯ СТОЛБЦОВ В ТАБЛИЦЕ -->
        <tr>
            <th>Наименование</th>
            <th class="actions">Действия</th>
        </tr>
    <!-- - - - - - - - - - - - - - -->
    </thead>
    <tbody class="table-rows">
        @include('partials.category-items')
            <!-- ================================================ -->
    </tbody>
    </table>

</div>
            </div>
        </div>

      <hr>

      <footer>
        <p>© JAANik 2018</p>
      </footer>
    </div> <!-- /container -->
    @endsection
