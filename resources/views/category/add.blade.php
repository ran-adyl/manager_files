@extends('layouts.main')
@section('content')
<div class="container">
    <!-- Example row of columns -->
    <div class="row">
        <div class="col-md-12">

            <div class="actions columns large-2 medium-3">
                <ul class="list-inline">
                    <li><a href="{{route('load_dir')}}" class="btn btn-xs btn-success"><i class="icon-level-down"></i>
                            Список Категорий</a></li>
                </ul>
            </div>
            <div class="returnItems form large-10 medium-9 columns">
                <form method="post" accept-charset="utf-8" role="form" action="{{route('store_dir')}}">
                    <div style="display:none;">
                        <input type="hidden" name="_method" value="POST"></div>
                    <fieldset>
                        <legend>Добавить Категорию</legend>

                        <input type="text" name="category" id="category" class="form-control"></div>

                        <input type="hidden" name="_token" value="{{csrf_token()}}">

                        <input type="hidden" name="user_id" value="1">
                        <!--                <input type="number" name="quantity_return_item" id="quantity-return-item" class="form-control"></div>-->
                    </fieldset>

                    <button type="submit" class="btn">Отправить</button>
                </form>
            </div>
        </div>
    </div>

    <hr>

    <footer>
        <label> © JAANik 2018 </label>
    </footer>
    <!-- /container -->

</div>
@endsection