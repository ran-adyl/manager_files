    @extends('layouts.main')
    @section('content')
    <div class="container">
        <!-- Example row of columns -->
        <div class="row">
            <div class="col-md-12">

                <div class="actions columns large-2 medium-3">
                <h3>Категория {{$category->name}}</h3>
    <ul class="list-inline">
<!--        <li><a href="http://manfiles.uz/fil_add?id=$category->id}}" class="btn btn-xs btn-primary"><i class="icon-plus"></i> Добавить Файл</a></li>-->
        <li><a href="{{route('add_file')}}?id={{$category->id}}" class="btn btn-xs btn-primary"><i class="icon-plus"></i> Добавить Файл</a></li>
        <li><a href="{{route('load_dir')}}" class="btn btn-xs btn-success"><i class="icon-plus"></i> Список Категорий</a></li>
    </ul>
</div>


<div class="fileItems index large-10 medium-9 columns">
    <table class="table table-striped">
    <thead>
    <!-- НАЗВАНИЯ СТОЛБЦОВ В ТАБЛИЦЕ -->
        <tr>
<!--            <th><a href="http://manfiles.uz">Наименование</a></th>-->
            <th>Наименование</th>
            <th class="actions">Действия</th>
        </tr>
    <!-- - - - - - - - - - - - - - -->
    </thead>
    <tbody class="table-rows">
        @include('partials.file-items')
            <!-- ================================================ -->
    </tbody>
    </table>

</div>            </div>
        </div>

      <hr>

      <footer>
        <p>© JAANik 2018</p>
      </footer>
    </div> <!-- /container -->
    @endsection
