@extends('layouts.main')
@section('content')
<div class="container">
    <!-- Example row of columns -->
    <div class="row">
        <div class="col-md-12">

            <div class="actions columns large-2 medium-3">
                <ul class="list-inline">
                    <li><a href="{{route('load_file')}}?id={{$category_id}}" class="btn btn-xs btn-success"><i class="icon-level-down"></i>
                            Список Файлов</a></li>
                </ul>
            </div>
            <div class="fileItems form large-10 medium-9 columns">
                <form method="post" accept-charset="utf-8" role="form" action="{{route('store_file')}}" enctype="multipart/form-data">
                    <div style="display:none;">
                        <input type="hidden" name="_method" value="POST"></div>
                    <fieldset>
                        <legend>Добавить Файл</legend>
                            <input size=40 type=file name="myfile"><br>
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="user_id" value="1">
                            <input type="hidden" name="dir_id" value="{{$category_id}}">
  <!--                         <input type="hidden" name="path" value=//$category->path}}>-->
                    </fieldset>

                    <button type="submit" class="btn">Отправить</button>
                </form>
            </div>
        </div>
    </div>

    <hr>

    <footer>
        <label> © JAANik 2018 </label>
    </footer>
    <!-- /container -->

</div>
@endsection