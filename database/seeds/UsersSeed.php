<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $user= User::firstOrCreate(
            ['email'=>'admin@admin.org'],
            ['name'=>'admin','password'=> bcrypt('password'),
            'created_at'=> date('Y-m-d'), 'updated_at' => date('Y-m-d')]
        );
    }
}
